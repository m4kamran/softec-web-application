// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  stripeKey: 'pk_test_70zDMjO0pkwBDQ3NQrqn8DAC',
  firebase: {
    apiKey: 'AIzaSyA22aPORWQ16laKW14JBGkxePLypD4pAzs',
    authDomain: 'softec-731c3.firebaseapp.com',
    databaseURL: 'https://softec-731c3.firebaseio.com',
    projectId: 'softec-731c3',
    storageBucket: 'softec-731c3.appspot.com',
    messagingSenderId: '956735393107'
  }
};
