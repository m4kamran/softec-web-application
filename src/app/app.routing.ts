import { Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { ManagerGuard } from './core/guards/manager.guard';

//Componenets
import { BlankComponent, ExecutiveLayout } from './shared/layouts';
import { LoginComponent } from './views/login/login.component';
import { NotFoundComponent } from './views/not-found/not-found.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: ExecutiveLayout,
    children: [
      {
        path: '',
        loadChildren: './views/user/user.module#UserModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/songs',
    pathMatch: 'full'
  },
  {
    path: '',
    component: BlankComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: '**',
        component: NotFoundComponent
      }
    ]
  }
];
