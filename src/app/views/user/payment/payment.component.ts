import { Component, OnInit, HostListener } from '@angular/core';
import { PaymentService } from '../../../core/services/payment.service';
import { environment } from '../../../../environments/environment';
import { AuthService } from '../../../core/authentication/auth.service';

@Component({
  selector: 'app-subscribe',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  handler: any;
  amount: number = 500;
  term: string = 'monthly';
  status: string;
  plan: string;

  constructor(public pmt: PaymentService, private auth: AuthService) {}

  ngOnInit() {
    this.auth.user.subscribe(user => {
      this.status = user.membership.status;
      this.plan = user.membership.plan;
    });
    this.configHandler();
  }

  private configHandler() {
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: '',
      locale: 'auto',
      token: token => this.pmt.processSubscription(token, this.term)
    });
  }

  openHandler(amount, term) {
    amount = amount * 100;
    this.term = term;
    this.handler.open({
      name: 'Hileets',
      description: 'Songs Subscription',
      amount: amount
    });
  }

  @HostListener('window:popstate')
  onpopstate() {
    this.handler.close();
  }
}
