import {
  Component,
  OnInit,
  ViewChild,
  NgZone,
  ElementRef
} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { pagesToggleService } from '../../../../shared/services/toggler.service';
import { EventsService } from '../../../../core/services/events.service';
import { AuthService } from '../../../../core/authentication/auth.service';
import { FormControl } from '@angular/forms';
// import {} from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { PaymentService } from '../../../../core/services/payment.service';
import { environment } from '../../../../../environments/environment';
@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnInit {
  eventForm: FormGroup;
  managerId: string;

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  public selectedLocation = [];

  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(
    private eventService: EventsService,
    private fb: FormBuilder,
    private toggler: pagesToggleService,
    private auth: AuthService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {}

  ngOnInit() {
    this.auth.user.subscribe(user => {
      this.managerId = user.uid;
    });

    this.eventForm = this.fb.group({
      name: ['kamran', Validators.required],
      startTime: ['', [Validators.required, Validators.minLength(6)]],
      endTime: ['', [Validators.required, Validators.email]],
      location: ['', Validators.required],
      managerId: [this.managerId, Validators.required]
    });

    // search
    this.zoom = 4;
    this.latitude = 39.8282;
    this.longitude = -98.5795;

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement,
        {
          types: ['address']
        }
      );
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          this.selectedLocation[0] = this.latitude;
          this.selectedLocation[1] = this.longitude;
          console.log(this.latitude, this.longitude);
        });
      });
    });
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  get name() {
    return this.eventForm.get('name');
  }

  get startTime() {
    return this.eventForm.get('startTime');
  }

  get endTime() {
    return this.eventForm.get('startTime');
  }

  get location() {
    return this.eventForm.get('location');
  }

  addEvent() {
    this.location.setValue(this.selectedLocation);
    this.eventService.addEvent(this.eventForm.value);
  }

  disabledDate(current: Date): boolean {
    //Future
    return current && current.getTime() < Date.now();
  }
}
