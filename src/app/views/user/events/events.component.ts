import { Component, OnInit, ViewChild } from '@angular/core';
import { pagesToggleService } from '../../../shared/services/toggler.service';
import { EventsService } from '../../../core/services/events.service';
import { Event } from '../../../core/models/Event';
import { PaymentService } from '../../../core/services/payment.service';
import { environment } from '../../../../environments/environment';
import { AuthService } from '../../../core/authentication/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  zoomLevel = 11;
  center = { lat: 33.6844, lng: 73.0479 };
  disableDefaultUI = true;
  showEventInfo = false;

  user;

  showAmbulances = false;
  showIncomingPatients = true;

  events: Event[];
  event: Event;

  showBooking = true;

  handler: any;
  amount: number = 500;

  distance;
  place;
  time;

  geolocationPosition;
  currentMarker = [];

  loader = false;
  styles = [
    {
      featureType: 'water',
      elementType: 'all',
      stylers: [
        {
          hue: '#e9ebed'
        },
        {
          saturation: -78
        },
        {
          lightness: 67
        },
        {
          visibility: 'simplified'
        }
      ]
    },
    {
      featureType: 'landscape',
      elementType: 'all',
      stylers: [
        {
          hue: '#ffffff'
        },
        {
          saturation: -100
        },
        {
          lightness: 100
        },
        {
          visibility: 'simplified'
        }
      ]
    },
    {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [
        {
          hue: '#bbc0c4'
        },
        {
          saturation: -93
        },
        {
          lightness: 31
        },
        {
          visibility: 'simplified'
        }
      ]
    },
    {
      featureType: 'poi',
      elementType: 'all',
      stylers: [
        {
          hue: '#ffffff'
        },
        {
          saturation: -100
        },
        {
          lightness: 100
        },
        {
          visibility: 'off'
        }
      ]
    },
    {
      featureType: 'road.local',
      elementType: 'geometry',
      stylers: [
        {
          hue: '#e9ebed'
        },
        {
          saturation: -90
        },
        {
          lightness: -8
        },
        {
          visibility: 'simplified'
        }
      ]
    },
    {
      featureType: 'transit',
      elementType: 'all',
      stylers: [
        {
          hue: '#e9ebed'
        },
        {
          saturation: 10
        },
        {
          lightness: 69
        },
        {
          visibility: 'on'
        }
      ]
    },
    {
      featureType: 'administrative.locality',
      elementType: 'all',
      stylers: [
        {
          hue: '#2c2e33'
        },
        {
          saturation: 7
        },
        {
          lightness: 19
        },
        {
          visibility: 'on'
        }
      ]
    },
    {
      featureType: 'road',
      elementType: 'labels',
      stylers: [
        {
          hue: '#bbc0c4'
        },
        {
          saturation: -93
        },
        {
          lightness: 31
        },
        {
          visibility: 'on'
        }
      ]
    },
    {
      featureType: 'road.arterial',
      elementType: 'labels',
      stylers: [
        {
          hue: '#bbc0c4'
        },
        {
          saturation: -93
        },
        {
          lightness: -2
        },
        {
          visibility: 'simplified'
        }
      ]
    }
  ];

  constructor(
    private toggler: pagesToggleService,
    private eventsService: EventsService,
    private pmt: PaymentService,
    private auth: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.auth.user.subscribe(user => {
      console.log(user);
      this.user = user;
    });

    this.configHandler();

    this.toggler.setBodyLayoutClass('no-header');
    this.toggler.setPageContainer('full-height');
    this.toggler.setContent('full-width full-height overlay-footer relative');
    setTimeout(() => {
      this.toggler.toggleFooter(false);
    });

    this.eventsService.getEvents().subscribe(events => {
      this.events = events;
      // this.events = events.filter(x => {
      //   return x.endTime.getUTCMilliseconds() > Date.now();
      // });
    });

    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          this.geolocationPosition = position;
          console.log(this.geolocationPosition);
          this.currentMarker[0] = position.coords.latitude;
          this.currentMarker[1] = position.coords.longitude;
        },
        error => {
          switch (error.code) {
            case 1:
              console.log('Permission Denied');
              break;
            case 2:
              console.log('Position Unavailable');
              break;
            case 3:
              console.log('Timeout');
              break;
          }
        }
      );
    }
  }

  zoomIn() {
    this.zoomLevel++;
  }

  zoomOut() {
    this.zoomLevel--;
  }

  onMarkerInit(marker) {
    console.log('marker', marker);
  }

  eventDetails(event) {
    this.loader = true;
    this.event = event;
    this.showEventInfo = true;
    let destination = {
      lat: event.location[0],
      lng: event.location[1]
    };
    let origin = {
      lat: this.geolocationPosition.coords.latitude,
      lng: this.geolocationPosition.coords.longitude
    };
    this.getDistance(origin, destination);
    event.members.forEach(element => {
      if (this.user.email == element.email) {
        this.showBooking = false;
      }
    });
  }

  // ambulanceInfo(ambulance) {
  //   this.ambulance = ambulance;
  //   this.showAmbulanceInfo = true;
  // }

  getDistance(origin, destination) {
    return new google.maps.DistanceMatrixService().getDistanceMatrix(
      {
        origins: [origin],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING
      },
      (results: any) => {
        this.distance = results.rows[0].elements[0].distance.text;
        this.place = results.originAddresses[0];
        this.time = results.rows[0].elements[0].duration.text;
        this.loader = false;
      }
    );
  }

  private configHandler() {
    this.handler = StripeCheckout.configure({
      key: environment.stripeKey,
      image: '',
      locale: 'auto',
      token: token => this.pmt.processCharge(token, this.amount).then()
    });
  }

  openHandler() {
    // const member = {
    //   name: this.user.name,
    //   email: this.user.email
    // };

    // this.event.members.push(member);
    this.handler.open({
      name: 'Hileets',
      description: 'Songs Subscription',
      amount: this.amount
    });
  }

  navigate() {
    this.router.navigate([`single-event/${this.event.uid}`]);
  }

  delete() {
    this.eventsService.deleteEvent(this.event.uid);
  }
}
