import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/authentication/auth.service';

declare const Twitch: any;
@Component({
  selector: 'app-live-streams',
  templateUrl: './live-streams.component.html',
  styleUrls: ['./live-streams.component.scss']
})
export class LiveStreamsComponent implements OnInit {
  status;
  plan;
  streams;
  constructor(private auth: AuthService) {}

  ngOnInit() {
    this.auth.user.subscribe(user => {
      this.status = user ? user.membership.status : undefined;
      this.plan = user ? user.membership.plan : undefined;
      this.streams = `<iframe
      src="https://player.twitch.tv/?channel=Monstercat&muted=true"
      height="720"
      width="1280"
      frameborder="0"
      scrolling="no"
      allowfullscreen="true">
  </iframe>`;
    });
  }
}
