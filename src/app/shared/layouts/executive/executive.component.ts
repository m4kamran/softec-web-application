import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RootLayout } from '../root/root.component';
import { AuthService } from '../../../core/authentication/auth.service';
import { pagesToggleService } from '../../services/toggler.service';
import { Router } from '@angular/router';
import { User } from '../../../core/models/User';
import { UsersService } from '../../../core/services/users.service';
declare var pg: any;

@Component({
  selector: 'executive-layout',
  templateUrl: './executive.component.html',
  styleUrls: ['./executive.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExecutiveLayout extends RootLayout implements OnInit {
  user: User;

  Role: string = 'User';
  menuItems = [
    {
      label: 'All Songs',
      routerLink: '/songs'
    },
    {
      label: 'Events List',
      routerLink: '/events'
    },
    {
      label: 'Live Streams',
      routerLink: '/liveStreams'
    },
    {
      label: 'Subscriptions',
      routerLink: '/subscriptions'
    }
  ];

  constructor(
    router: Router,
    toggler: pagesToggleService,
    private authService: AuthService,
    private usersService: UsersService
  ) {
    super(toggler, router);
  }

  ngOnInit() {
    pg.isHorizontalLayout = true;
    this.changeLayout('horizontal-menu');
    this.changeLayout('horizontal-app-menu');

    this.authService.user.subscribe(user => {
      this.user = user;
    });
  }

  signOut() {
    this.authService.signOut();
  }

  updateRole(type) {
    let User = {
      roles: {
        manager: true,
        user: true,
        producer: true,
        singer: false
      }
    };
    if (type == 'Manager') {
      this.Role = 'Manager';
      User = {
        roles: {
          manager: true,
          user: false,
          producer: false,
          singer: false
        }
      };
    }
    if (type == 'Singer') {
      this.Role = 'Singer';
      User = {
        roles: {
          manager: false,
          user: false,
          producer: false,
          singer: true
        }
      };
    }
    if (type == 'Producer') {
      this.Role = 'Producer';
      User = {
        roles: {
          manager: false,
          user: false,
          producer: true,
          singer: false
        }
      };
    }
    if (type == 'User') {
      this.Role = 'User';
      User = {
        roles: {
          manager: false,
          user: true,
          producer: false,
          singer: false
        }
      };
    }

    this.usersService.updateUser(this.user.uid, User);
  }
}
