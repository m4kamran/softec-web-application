import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../../core/authentication/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  userId: string;
  membership: any;

  constructor(
    private db: AngularFireDatabase,
    private store: AngularFirestore,
    private auth: AuthService
  ) {
    this.auth.user.subscribe(user => {
      this.userId = user.uid;
      console.log(this.userId);

      this.membership = this.db
        .object(`users/${user.uid}/pro-membership`)
        .valueChanges();
    });
  }

  processSubscription(token: any, term: string) {
    return this.db
      .object(`/users/${this.userId}/membership`)
      .update({ token: token.id, term: term });
  }

  processCharge(token: any, amount) {
    console.log(token, amount, 'AMT', this.userId);
    const payment = { token, amount };
    return this.db.list(`/payments/${this.userId}`).push(payment);
  }
}
