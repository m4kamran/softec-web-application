import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { MessageService } from '../../shared/components/message/message.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  UsersCollection: AngularFirestoreCollection<User>;
  UserDocument: AngularFirestoreDocument<User>;

  ROOT_URL: string = 'https://us-central1-salama-1337.cloudfunctions.net/api';

  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache'
  });

  options = {
    headers: this.httpHeaders
  };

  constructor(
    private afs: AngularFirestore,
    private _notification: MessageService,
    private router: Router,
    private http: HttpClient
  ) {}

  //Get users list
  getUsers(): Observable<User[]> {
    this.UsersCollection = this.afs.collection('users');
    return this.UsersCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as User;
        data.uid = action.payload.doc.id;
        return data;
      });
    });
  }

  //Get single user
  getUser(id: string): Observable<User> {
    this.UserDocument = this.afs.doc<User>(`users/${id}`);
    const practitioner = this.UserDocument.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as User;
        data.uid = action.payload.id;
        return data;
      }
    });

    return practitioner;
  }

  //Create users for Authentication.
  createAuthUser(email: string, password: string): Observable<any> {
    const data = {
      email: email,
      password: password
    };

    return this.http.post(
      this.ROOT_URL + '/createUser',
      JSON.stringify(data),
      this.options
    );
  }

  // Delete users from Authentication
  deleteAuthUser(id: string): Observable<any> {
    return this.http.delete(this.ROOT_URL + '/deleteUser/' + id, this.options);
  }

  // Create User record in database
  createUser(user: User): any {
    return this.createAuthUser(user.email, user.password).subscribe(
      res => {
        delete user.password;
        this.UserDocument = this.afs.collection('users').doc(res.uid);
        this.UserDocument.set(user)
          .then(() => {
            this._notification.create('success', `User Created Successfully`, {
              Title: user.displayName,
              imgURL: user.photoURL,
              Position: 'top-right',
              Style: 'circle',
              Duration: 2000
            });
          })
          .catch(err => {
            this._notification.create(
              'danger',
              `User not created. Error ${err.message}`,
              {
                Position: 'top',
                Style: 'bar',
                Duration: 2000
              }
            );
          });
      },
      res => {
        this._notification.create(
          'danger',
          `User not created. Error. ${res.error.message}`,
          {
            Position: 'top',
            Style: 'bar',
            Duration: 2000
          }
        );
      }
    );
  }

  //Delete users from database
  deleteUser(id: string): void {
    this.deleteAuthUser(id).subscribe(
      () => {
        this.UserDocument = this.afs.doc<User>(`users/${id}`);
        this.UserDocument.delete()
          .then(() => {
            this._notification.create('danger', `User Deleted Successfully`, {
              Position: 'top',
              Style: 'bar',
              Duration: 2000
            });
            this.router.navigateByUrl('/manage-users');
          })
          .catch(err => {
            this._notification.create(
              'danger',
              `User not deleted. Error ${err.message}`,
              {
                Position: 'top',
                Style: 'bar',
                Duration: 2000
              }
            );
          });
      },
      res => {
        this._notification.create(
          'danger',
          `User not deleted. Error. ${res.error.message}`,
          {
            Position: 'top',
            Style: 'bar',
            Duration: 2000
          }
        );
      }
    );
  }

  updateUser(id: string, user: User) {
    return this.afs
      .collection('users')
      .doc(id)
      .update(user)
      .then(() => {
        console.log(user);
        console.log('user updated!');
      });
  }
}
