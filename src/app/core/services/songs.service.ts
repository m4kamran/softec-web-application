import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { MessageService } from '../../shared/components/message/message.service';
import { Song } from '../models/Song';

@Injectable({
  providedIn: 'root'
})
export class SongsService {
  SongsCollection: AngularFirestoreCollection<Song>;
  SongDocument: AngularFirestoreDocument<Song>;

  constructor(
    private afs: AngularFirestore,
    private _notification: MessageService
  ) {}

  addSong(song: Song) {
    return this.afs
      .collection('songs')
      .add(song)
      .then(() => {
        this._notification.create('success', `Song Added Successfully`, {
          Position: 'top-right',
          Style: 'bar',
          Duration: 2000
        });
      });
  }

  getSongs(): Observable<Song[]> {
    this.SongsCollection = this.afs.collection('songs');
    return this.SongsCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Song;
        data.uid = action.payload.doc.id;
        return data;
      });
    });
  }
}
