import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { MessageService } from '../../shared/components/message/message.service';
import { Event } from '../models/Event';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  EventsCollection: AngularFirestoreCollection<Event>;
  EventDocument: AngularFirestoreDocument<Event>;

  constructor(
    private afs: AngularFirestore,
    private _notification: MessageService
  ) {}

  getEvents(): Observable<Event[]> {
    this.EventsCollection = this.afs.collection('events');
    return this.EventsCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Event;
        data.uid = action.payload.doc.id;
        return data;
      });
    });
  }

  getEvent(id: string): Observable<Event> {
    this.EventDocument = this.afs.doc<Event>(`events/${id}`);
    const patient = this.EventDocument.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as Event;
        data.uid = action.payload.id;
        return data;
      }
    });
    return patient;
  }

  addEvent(event: Event) {
    this.afs
      .collection('events')
      .add(event)
      .then(() => {
        this._notification.create('success', `Event Added Successfully`, {
          Position: 'top-right',
          Style: 'bar',
          Duration: 2000
        });
      });
    // .then(() => {
    //   this.afs
    //     .collection('notifications')
    //     .add({
    //       title: 'Event incoming',
    //       body: 'An event has been generated near your!'
    //     });
    // });
  }

  updateEvent(id: string, event: Event) {
    return this.afs
      .collection('events')
      .doc(id)
      .update(event)
      .then(() => {
        console.log(event);
        console.log('event updated!');
      });
  }

  deleteEvent(id: string) {
    return this.afs
      .collection('events')
      .doc(id)
      .delete().then(() => {
        this._notification.create('danger', 'Event deleted succcess fully', {
          Title: 'Event deleted',
          imgURL: '../../../assets/img/patient.svg',
          Position: 'top-right',
          Style: 'circle',
          Duration: 5000
        });
      })
  }
}
