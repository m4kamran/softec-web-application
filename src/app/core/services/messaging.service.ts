import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';

import 'rxjs/add/operator/take';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { MessageService } from '../../shared/components/message/message.service';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
  messaging = firebase.messaging();

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth,
    private _notification: MessageService
  ) {}

  updateToken(token) {
    this.afAuth.authState.take(1).subscribe(user => {
      if (!user) return;

      this.db
        .collection('users')
        .doc(user.uid)
        .update({ token: token });
    });
  }

  getPermission() {
    this.messaging
      .requestPermission()
      .then(() => {
        console.log('permission granted');
        return this.messaging.getToken();
      })
      .then(token => {
        console.log(token);
        this.updateToken(token);
      })
      .catch(err => {
        console.log('Unable to get permission to notify.', err);
      });
  }

  receiveMessage() {
    console.log('hi');
    this.messaging.onMessage(payload => {
      console.log(payload);
      // this.playNotificationSound();
      this._notification.create('danger', payload.notification.body, {
        Title: payload.notification.title,
        imgURL: '../../../assets/img/patient.svg',
        Position: 'top-right',
        Style: 'circle',
        Duration: 5000
      });
      console.log('Message received. ', payload);
    });
  }

  playNotificationSound() {
    let audio = new Audio();
    audio.src = '../../../assets/sounds/notification.mp3';
    audio.load();
    audio.play();
  }
}
